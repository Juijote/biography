vbox = {
	name = "chronicle_event_window_widget_control_data"
	layoutpolicy_horizontal = expanding

	# Main text area

	vbox = {
		layoutpolicy_horizontal = expanding
		margin_left = 10
		margin_bottom = 10

		# For compatibility with Resizable Events mod

		datacontext = "[GetScriptedGui('big_options')]"
		visible = "[Not(ScriptedGui.IsShown(GuiScope.SetRoot(GetPlayer.MakeScope).End))]"

		# Standard

		chronicle_event_window_widget_scroll_area_data = {

			blockoverride "chronicle_event_window_widget_scroll_area_visible"
			{
				visible = "[And( Not(GetVariableSystem.Exists( 'chronicle_widget_control_reverse_order' )), Not(GetVariableSystem.Exists( 'chronicle_widget_control_simple_mode' )) )]"
			}

			blockoverride "chronicle_event_window_widget_scroll_area_text"
			{
				text = "chronicle_data_text"
			}

		}

		# Reverse order

		chronicle_event_window_widget_scroll_area_data = {

			blockoverride "chronicle_event_window_widget_scroll_area_visible"
			{
				visible = "[And( GetVariableSystem.Exists( 'chronicle_widget_control_reverse_order' ), Not(GetVariableSystem.Exists( 'chronicle_widget_control_simple_mode' )) )]"
			}

			blockoverride "chronicle_event_window_widget_scroll_area_text"
			{
				text = "chronicle_data_text_reverse"
			}

		}

		# Simple mode

		chronicle_event_window_widget_scroll_area_data = {

			blockoverride "chronicle_event_window_widget_scroll_area_visible"
			{
				visible = "[And( Not(GetVariableSystem.Exists( 'chronicle_widget_control_reverse_order' )), GetVariableSystem.Exists( 'chronicle_widget_control_simple_mode' ) )]"
			}

			blockoverride "chronicle_event_window_widget_scroll_area_text"
			{
				text = "chronicle_data_text_simple"
			}

		}

		# Reverse order + Simple mode

		chronicle_event_window_widget_scroll_area_data = {

			blockoverride "chronicle_event_window_widget_scroll_area_visible"
			{
				visible = "[And( GetVariableSystem.Exists( 'chronicle_widget_control_reverse_order' ), GetVariableSystem.Exists( 'chronicle_widget_control_simple_mode' ) )]"
			}

			blockoverride "chronicle_event_window_widget_scroll_area_text"
			{
				text = "chronicle_data_text_simple_reverse"
			}

		}

	}


	# Lower controls

	hbox = {
		layoutpolicy_horizontal = expanding
		margin_left = 10
		spacing = 5

		expand = {}


		# Reverse order

		button_standard = {
			size = { 30 30 }
			onclick = "[GetVariableSystem.Toggle( 'chronicle_widget_control_reverse_order' )]"
			tooltip = "chronicle_translation_option_reverse_order"
			using = tooltip_ne

			button_icon = {
				alwaystransparent = yes
				parentanchor = center
				size = { 30 30 }
				texture = "gfx/interface/icons/flat_icons/sort_icon.dds"
				framesize = { 66 66 }
				frame = "[BoolTo1And2( GetVariableSystem.Exists( 'chronicle_widget_control_reverse_order' ) )]"

				blockoverride "button_frames"
				{
					effectname = "NoHighlight"
				}
			}
		}


		# Simple mode

		button_standard = {
			size = { 30 30 }
			visible = "[GetVariableSystem.Exists( 'chronicle_widget_control_simple_mode' )]"
			onclick = "[GetVariableSystem.Toggle( 'chronicle_widget_control_simple_mode' )]"
			tooltip = "chronicle_translation_option_show_more"
			using = tooltip_ne

			button_plus = {
				alwaystransparent = yes
				parentanchor = center
				size = { 25 25 }
			}
		}

		button_standard = {
			size = { 30 30 }
			visible = "[Not( GetVariableSystem.Exists( 'chronicle_widget_control_simple_mode' ) )]"
			onclick = "[GetVariableSystem.Toggle( 'chronicle_widget_control_simple_mode' )]"
			tooltip = "chronicle_translation_option_show_less"
			using = tooltip_ne

			button_minus = {
				alwaystransparent = yes
				parentanchor = center
				size = { 25 25 }
			}
		}
	}

	expand = {}

}


types ChronicleEventWindowWidgetControlData
{
	type chronicle_event_window_widget_scroll_area_data = scrollarea {
		datacontext = "[EventWindow.GetPortraitCharacter('left_portrait')]"

		block "chronicle_event_window_widget_scroll_area_visible" {}

		layoutpolicy_horizontal = expanding
		layoutpolicy_vertical = expanding
		size = { 470 380 }

		scrollbarpolicy_horizontal = always_off

		scrollbar_vertical = {
			using = Scrollbar_Vertical

			blockoverride "alpha" {
				alpha = 0.6
			}
		}

		scrollwidget = {
			text_multi = {
				size = { 470 380 }
				autoresize = yes

				block "chronicle_event_window_widget_scroll_area_text" {}

				fontsize = 16
			}
		}
	}
}
