﻿chronicle_scripted_gui_show_chronicle = {
	scope = character

	saved_scopes = {
		actor
	}

	effect = {
		save_scope_as = recipient
		scope:actor = {
			trigger_event = chronicle.0001
		}
	}
}

chronicle_scripted_gui_show_chronicle_filter = {
	scope = character

	saved_scopes = {
		actor
	}

	effect = {
		save_scope_as = recipient
		scope:actor = {
			open_interaction_window = {
				interaction = chronicle_interaction
				actor = scope:actor
				recipient = scope:recipient
			}
		}
	}
}

chronicle_scripted_gui_show_chronicle_edit_free_text = {
	scope = character

	saved_scopes = {
		actor
	}

	effect = {
		save_scope_as = recipient
		scope:recipient = {
			add_character_flag = chronicle_free_text_exists
		}
	}
}

chronicle_scripted_gui_show_chronicle_clear_free_text = {
	scope = character

	saved_scopes = {
		actor
	}

	effect = {
		save_scope_as = recipient
		scope:recipient = {
			remove_localized_text = chronicle_free_text
			remove_character_flag = chronicle_free_text_exists
		}
	}
}
