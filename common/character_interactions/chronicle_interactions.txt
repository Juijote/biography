﻿chronicle_interaction = {

	category = interaction_category_friendly
	common_interaction = yes

	auto_accept = yes
	use_diplomatic_range = no
	ignores_pending_interaction_block = yes

	icon = _missing_interaction
	desc = chronicle_interaction_text
	send_name = chronicle_interaction_popup

	is_shown = {
		exists = global_var:chronicle_mod_version
		global_var:chronicle_mod_version >= 1.20
	}

	options_heading = chronicle_interaction_heading

	send_options_exclusive = no

	send_option = {
		flag = filter_title_primary
		starts_enabled = { chronicle_is_filter_selected_trigger = { FILTER = title_primary } }
		can_be_changed = { always = yes	}
		localization = chronicle_interaction_filter_title_primary
	}

	send_option = {
		flag = filter_title_gain_loss
		starts_enabled = { chronicle_is_filter_selected_trigger = { FILTER = title_gain_loss } }
		can_be_changed = { always = yes	}
		localization = chronicle_interaction_filter_title_gain_loss
	}

	send_option = {
		flag = filter_war_leader
		starts_enabled = { chronicle_is_filter_selected_trigger = { FILTER = war_leader } }
		can_be_changed = { always = yes	}
		localization = chronicle_interaction_filter_war_leader
	}

	send_option = {
		flag = filter_war_ally
		starts_enabled = { chronicle_is_filter_selected_trigger = { FILTER = war_ally } }
		can_be_changed = { always = yes	}
		localization = chronicle_interaction_filter_war_ally
	}

	send_option = {
		flag = filter_trait
		starts_enabled = { chronicle_is_filter_selected_trigger = { FILTER = trait } }
		can_be_changed = { always = yes	}
		localization = chronicle_interaction_filter_trait
	}

	send_option = {
		flag = filter_family
		starts_enabled = { chronicle_is_filter_selected_trigger = { FILTER = family } }
		can_be_changed = { always = yes	}
		localization = chronicle_interaction_filter_family
	}

	send_option = {
		flag = filter_relation
		starts_enabled = { chronicle_is_filter_selected_trigger = { FILTER = relation } }
		can_be_changed = { always = yes	}
		localization = chronicle_interaction_filter_relation
	}

	send_option = {
		flag = option_save_setting
		starts_enabled = { always = no }
		can_be_changed = { always = yes	}
		is_shown = { NOT = { exists = scope:secondary_recipient } }
		localization = chronicle_interaction_option_save_setting
	}

	send_option = {
		flag = option_save_setting_all
		starts_enabled = { always = no }
		can_be_changed = { always = yes	}
		is_shown = { NOT = { exists = scope:secondary_recipient } }
		localization = chronicle_interaction_option_save_setting_all
	}

	on_accept = {
		scope:actor = {
			if = {
				limit = {
					NOT = { exists = scope:secondary_recipient }
				}
				trigger_event = chronicle.0001
			}
			else = {
				trigger_event = chronicle.0007
			}
		}
	}

}
