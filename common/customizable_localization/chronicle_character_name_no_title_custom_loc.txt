﻿ChronicleCharacterNameNoTitle1 = {
	type = character

	text = {
		trigger = {
			exists = var:char1
			NOT = { var:char1 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.1
	}
	text = {
		trigger = {
			NOT = { exists = var:char1 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle2 = {
	type = character

	text = {
		trigger = {
			exists = var:char2
			NOT = { var:char2 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.2
	}
	text = {
		trigger = {
			NOT = { exists = var:char2 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle3 = {
	type = character

	text = {
		trigger = {
			exists = var:char3
			NOT = { var:char3 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.3
	}
	text = {
		trigger = {
			NOT = { exists = var:char3 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle4 = {
	type = character

	text = {
		trigger = {
			exists = var:char4
			NOT = { var:char4 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.4
	}
	text = {
		trigger = {
			NOT = { exists = var:char4 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle5 = {
	type = character

	text = {
		trigger = {
			exists = var:char5
			NOT = { var:char5 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.5
	}
	text = {
		trigger = {
			NOT = { exists = var:char5 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle6 = {
	type = character

	text = {
		trigger = {
			exists = var:char6
			NOT = { var:char6 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.6
	}
	text = {
		trigger = {
			NOT = { exists = var:char6 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle7 = {
	type = character

	text = {
		trigger = {
			exists = var:char7
			NOT = { var:char7 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.7
	}
	text = {
		trigger = {
			NOT = { exists = var:char7 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle8 = {
	type = character

	text = {
		trigger = {
			exists = var:char8
			NOT = { var:char8 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.8
	}
	text = {
		trigger = {
			NOT = { exists = var:char8 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle9 = {
	type = character

	text = {
		trigger = {
			exists = var:char9
			NOT = { var:char9 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.9
	}
	text = {
		trigger = {
			NOT = { exists = var:char9 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle10 = {
	type = character

	text = {
		trigger = {
			exists = var:char10
			NOT = { var:char10 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.10
	}
	text = {
		trigger = {
			NOT = { exists = var:char10 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle11 = {
	type = character

	text = {
		trigger = {
			exists = var:char11
			NOT = { var:char11 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.11
	}
	text = {
		trigger = {
			NOT = { exists = var:char11 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle12 = {
	type = character

	text = {
		trigger = {
			exists = var:char12
			NOT = { var:char12 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.12
	}
	text = {
		trigger = {
			NOT = { exists = var:char12 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle13 = {
	type = character

	text = {
		trigger = {
			exists = var:char13
			NOT = { var:char13 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.13
	}
	text = {
		trigger = {
			NOT = { exists = var:char13 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle14 = {
	type = character

	text = {
		trigger = {
			exists = var:char14
			NOT = { var:char14 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.14
	}
	text = {
		trigger = {
			NOT = { exists = var:char14 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle15 = {
	type = character

	text = {
		trigger = {
			exists = var:char15
			NOT = { var:char15 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.15
	}
	text = {
		trigger = {
			NOT = { exists = var:char15 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle16 = {
	type = character

	text = {
		trigger = {
			exists = var:char16
			NOT = { var:char16 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.16
	}
	text = {
		trigger = {
			NOT = { exists = var:char16 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle17 = {
	type = character

	text = {
		trigger = {
			exists = var:char17
			NOT = { var:char17 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.17
	}
	text = {
		trigger = {
			NOT = { exists = var:char17 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle18 = {
	type = character

	text = {
		trigger = {
			exists = var:char18
			NOT = { var:char18 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.18
	}
	text = {
		trigger = {
			NOT = { exists = var:char18 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle19 = {
	type = character

	text = {
		trigger = {
			exists = var:char19
			NOT = { var:char19 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.19
	}
	text = {
		trigger = {
			NOT = { exists = var:char19 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle20 = {
	type = character

	text = {
		trigger = {
			exists = var:char20
			NOT = { var:char20 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.20
	}
	text = {
		trigger = {
			NOT = { exists = var:char20 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle21 = {
	type = character

	text = {
		trigger = {
			exists = var:char21
			NOT = { var:char21 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.21
	}
	text = {
		trigger = {
			NOT = { exists = var:char21 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle22 = {
	type = character

	text = {
		trigger = {
			exists = var:char22
			NOT = { var:char22 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.22
	}
	text = {
		trigger = {
			NOT = { exists = var:char22 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle23 = {
	type = character

	text = {
		trigger = {
			exists = var:char23
			NOT = { var:char23 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.23
	}
	text = {
		trigger = {
			NOT = { exists = var:char23 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle24 = {
	type = character

	text = {
		trigger = {
			exists = var:char24
			NOT = { var:char24 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.24
	}
	text = {
		trigger = {
			NOT = { exists = var:char24 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle25 = {
	type = character

	text = {
		trigger = {
			exists = var:char25
			NOT = { var:char25 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.25
	}
	text = {
		trigger = {
			NOT = { exists = var:char25 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle26 = {
	type = character

	text = {
		trigger = {
			exists = var:char26
			NOT = { var:char26 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.26
	}
	text = {
		trigger = {
			NOT = { exists = var:char26 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle27 = {
	type = character

	text = {
		trigger = {
			exists = var:char27
			NOT = { var:char27 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.27
	}
	text = {
		trigger = {
			NOT = { exists = var:char27 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle28 = {
	type = character

	text = {
		trigger = {
			exists = var:char28
			NOT = { var:char28 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.28
	}
	text = {
		trigger = {
			NOT = { exists = var:char28 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle29 = {
	type = character

	text = {
		trigger = {
			exists = var:char29
			NOT = { var:char29 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.29
	}
	text = {
		trigger = {
			NOT = { exists = var:char29 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle30 = {
	type = character

	text = {
		trigger = {
			exists = var:char30
			NOT = { var:char30 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.30
	}
	text = {
		trigger = {
			NOT = { exists = var:char30 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle31 = {
	type = character

	text = {
		trigger = {
			exists = var:char31
			NOT = { var:char31 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.31
	}
	text = {
		trigger = {
			NOT = { exists = var:char31 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle32 = {
	type = character

	text = {
		trigger = {
			exists = var:char32
			NOT = { var:char32 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.32
	}
	text = {
		trigger = {
			NOT = { exists = var:char32 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle33 = {
	type = character

	text = {
		trigger = {
			exists = var:char33
			NOT = { var:char33 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.33
	}
	text = {
		trigger = {
			NOT = { exists = var:char33 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle34 = {
	type = character

	text = {
		trigger = {
			exists = var:char34
			NOT = { var:char34 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.34
	}
	text = {
		trigger = {
			NOT = { exists = var:char34 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle35 = {
	type = character

	text = {
		trigger = {
			exists = var:char35
			NOT = { var:char35 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.35
	}
	text = {
		trigger = {
			NOT = { exists = var:char35 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle36 = {
	type = character

	text = {
		trigger = {
			exists = var:char36
			NOT = { var:char36 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.36
	}
	text = {
		trigger = {
			NOT = { exists = var:char36 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle37 = {
	type = character

	text = {
		trigger = {
			exists = var:char37
			NOT = { var:char37 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.37
	}
	text = {
		trigger = {
			NOT = { exists = var:char37 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle38 = {
	type = character

	text = {
		trigger = {
			exists = var:char38
			NOT = { var:char38 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.38
	}
	text = {
		trigger = {
			NOT = { exists = var:char38 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle39 = {
	type = character

	text = {
		trigger = {
			exists = var:char39
			NOT = { var:char39 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.39
	}
	text = {
		trigger = {
			NOT = { exists = var:char39 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle40 = {
	type = character

	text = {
		trigger = {
			exists = var:char40
			NOT = { var:char40 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.40
	}
	text = {
		trigger = {
			NOT = { exists = var:char40 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle41 = {
	type = character

	text = {
		trigger = {
			exists = var:char41
			NOT = { var:char41 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.41
	}
	text = {
		trigger = {
			NOT = { exists = var:char41 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle42 = {
	type = character

	text = {
		trigger = {
			exists = var:char42
			NOT = { var:char42 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.42
	}
	text = {
		trigger = {
			NOT = { exists = var:char42 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle43 = {
	type = character

	text = {
		trigger = {
			exists = var:char43
			NOT = { var:char43 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.43
	}
	text = {
		trigger = {
			NOT = { exists = var:char43 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle44 = {
	type = character

	text = {
		trigger = {
			exists = var:char44
			NOT = { var:char44 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.44
	}
	text = {
		trigger = {
			NOT = { exists = var:char44 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle45 = {
	type = character

	text = {
		trigger = {
			exists = var:char45
			NOT = { var:char45 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.45
	}
	text = {
		trigger = {
			NOT = { exists = var:char45 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle46 = {
	type = character

	text = {
		trigger = {
			exists = var:char46
			NOT = { var:char46 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.46
	}
	text = {
		trigger = {
			NOT = { exists = var:char46 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle47 = {
	type = character

	text = {
		trigger = {
			exists = var:char47
			NOT = { var:char47 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.47
	}
	text = {
		trigger = {
			NOT = { exists = var:char47 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle48 = {
	type = character

	text = {
		trigger = {
			exists = var:char48
			NOT = { var:char48 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.48
	}
	text = {
		trigger = {
			NOT = { exists = var:char48 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle49 = {
	type = character

	text = {
		trigger = {
			exists = var:char49
			NOT = { var:char49 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.49
	}
	text = {
		trigger = {
			NOT = { exists = var:char49 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle50 = {
	type = character

	text = {
		trigger = {
			exists = var:char50
			NOT = { var:char50 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.50
	}
	text = {
		trigger = {
			NOT = { exists = var:char50 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle51 = {
	type = character

	text = {
		trigger = {
			exists = var:char51
			NOT = { var:char51 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.51
	}
	text = {
		trigger = {
			NOT = { exists = var:char51 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle52 = {
	type = character

	text = {
		trigger = {
			exists = var:char52
			NOT = { var:char52 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.52
	}
	text = {
		trigger = {
			NOT = { exists = var:char52 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle53 = {
	type = character

	text = {
		trigger = {
			exists = var:char53
			NOT = { var:char53 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.53
	}
	text = {
		trigger = {
			NOT = { exists = var:char53 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle54 = {
	type = character

	text = {
		trigger = {
			exists = var:char54
			NOT = { var:char54 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.54
	}
	text = {
		trigger = {
			NOT = { exists = var:char54 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle55 = {
	type = character

	text = {
		trigger = {
			exists = var:char55
			NOT = { var:char55 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.55
	}
	text = {
		trigger = {
			NOT = { exists = var:char55 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle56 = {
	type = character

	text = {
		trigger = {
			exists = var:char56
			NOT = { var:char56 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.56
	}
	text = {
		trigger = {
			NOT = { exists = var:char56 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle57 = {
	type = character

	text = {
		trigger = {
			exists = var:char57
			NOT = { var:char57 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.57
	}
	text = {
		trigger = {
			NOT = { exists = var:char57 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle58 = {
	type = character

	text = {
		trigger = {
			exists = var:char58
			NOT = { var:char58 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.58
	}
	text = {
		trigger = {
			NOT = { exists = var:char58 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle59 = {
	type = character

	text = {
		trigger = {
			exists = var:char59
			NOT = { var:char59 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.59
	}
	text = {
		trigger = {
			NOT = { exists = var:char59 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle60 = {
	type = character

	text = {
		trigger = {
			exists = var:char60
			NOT = { var:char60 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.60
	}
	text = {
		trigger = {
			NOT = { exists = var:char60 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle61 = {
	type = character

	text = {
		trigger = {
			exists = var:char61
			NOT = { var:char61 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.61
	}
	text = {
		trigger = {
			NOT = { exists = var:char61 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle62 = {
	type = character

	text = {
		trigger = {
			exists = var:char62
			NOT = { var:char62 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.62
	}
	text = {
		trigger = {
			NOT = { exists = var:char62 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle63 = {
	type = character

	text = {
		trigger = {
			exists = var:char63
			NOT = { var:char63 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.63
	}
	text = {
		trigger = {
			NOT = { exists = var:char63 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle64 = {
	type = character

	text = {
		trigger = {
			exists = var:char64
			NOT = { var:char64 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.64
	}
	text = {
		trigger = {
			NOT = { exists = var:char64 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle65 = {
	type = character

	text = {
		trigger = {
			exists = var:char65
			NOT = { var:char65 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.65
	}
	text = {
		trigger = {
			NOT = { exists = var:char65 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle66 = {
	type = character

	text = {
		trigger = {
			exists = var:char66
			NOT = { var:char66 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.66
	}
	text = {
		trigger = {
			NOT = { exists = var:char66 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle67 = {
	type = character

	text = {
		trigger = {
			exists = var:char67
			NOT = { var:char67 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.67
	}
	text = {
		trigger = {
			NOT = { exists = var:char67 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle68 = {
	type = character

	text = {
		trigger = {
			exists = var:char68
			NOT = { var:char68 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.68
	}
	text = {
		trigger = {
			NOT = { exists = var:char68 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle69 = {
	type = character

	text = {
		trigger = {
			exists = var:char69
			NOT = { var:char69 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.69
	}
	text = {
		trigger = {
			NOT = { exists = var:char69 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle70 = {
	type = character

	text = {
		trigger = {
			exists = var:char70
			NOT = { var:char70 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.70
	}
	text = {
		trigger = {
			NOT = { exists = var:char70 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle71 = {
	type = character

	text = {
		trigger = {
			exists = var:char71
			NOT = { var:char71 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.71
	}
	text = {
		trigger = {
			NOT = { exists = var:char71 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle72 = {
	type = character

	text = {
		trigger = {
			exists = var:char72
			NOT = { var:char72 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.72
	}
	text = {
		trigger = {
			NOT = { exists = var:char72 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle73 = {
	type = character

	text = {
		trigger = {
			exists = var:char73
			NOT = { var:char73 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.73
	}
	text = {
		trigger = {
			NOT = { exists = var:char73 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle74 = {
	type = character

	text = {
		trigger = {
			exists = var:char74
			NOT = { var:char74 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.74
	}
	text = {
		trigger = {
			NOT = { exists = var:char74 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle75 = {
	type = character

	text = {
		trigger = {
			exists = var:char75
			NOT = { var:char75 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.75
	}
	text = {
		trigger = {
			NOT = { exists = var:char75 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle76 = {
	type = character

	text = {
		trigger = {
			exists = var:char76
			NOT = { var:char76 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.76
	}
	text = {
		trigger = {
			NOT = { exists = var:char76 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle77 = {
	type = character

	text = {
		trigger = {
			exists = var:char77
			NOT = { var:char77 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.77
	}
	text = {
		trigger = {
			NOT = { exists = var:char77 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle78 = {
	type = character

	text = {
		trigger = {
			exists = var:char78
			NOT = { var:char78 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.78
	}
	text = {
		trigger = {
			NOT = { exists = var:char78 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle79 = {
	type = character

	text = {
		trigger = {
			exists = var:char79
			NOT = { var:char79 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.79
	}
	text = {
		trigger = {
			NOT = { exists = var:char79 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle80 = {
	type = character

	text = {
		trigger = {
			exists = var:char80
			NOT = { var:char80 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.80
	}
	text = {
		trigger = {
			NOT = { exists = var:char80 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle81 = {
	type = character

	text = {
		trigger = {
			exists = var:char81
			NOT = { var:char81 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.81
	}
	text = {
		trigger = {
			NOT = { exists = var:char81 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle82 = {
	type = character

	text = {
		trigger = {
			exists = var:char82
			NOT = { var:char82 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.82
	}
	text = {
		trigger = {
			NOT = { exists = var:char82 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle83 = {
	type = character

	text = {
		trigger = {
			exists = var:char83
			NOT = { var:char83 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.83
	}
	text = {
		trigger = {
			NOT = { exists = var:char83 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle84 = {
	type = character

	text = {
		trigger = {
			exists = var:char84
			NOT = { var:char84 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.84
	}
	text = {
		trigger = {
			NOT = { exists = var:char84 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle85 = {
	type = character

	text = {
		trigger = {
			exists = var:char85
			NOT = { var:char85 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.85
	}
	text = {
		trigger = {
			NOT = { exists = var:char85 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle86 = {
	type = character

	text = {
		trigger = {
			exists = var:char86
			NOT = { var:char86 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.86
	}
	text = {
		trigger = {
			NOT = { exists = var:char86 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle87 = {
	type = character

	text = {
		trigger = {
			exists = var:char87
			NOT = { var:char87 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.87
	}
	text = {
		trigger = {
			NOT = { exists = var:char87 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle88 = {
	type = character

	text = {
		trigger = {
			exists = var:char88
			NOT = { var:char88 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.88
	}
	text = {
		trigger = {
			NOT = { exists = var:char88 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle89 = {
	type = character

	text = {
		trigger = {
			exists = var:char89
			NOT = { var:char89 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.89
	}
	text = {
		trigger = {
			NOT = { exists = var:char89 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle90 = {
	type = character

	text = {
		trigger = {
			exists = var:char90
			NOT = { var:char90 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.90
	}
	text = {
		trigger = {
			NOT = { exists = var:char90 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle91 = {
	type = character

	text = {
		trigger = {
			exists = var:char91
			NOT = { var:char91 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.91
	}
	text = {
		trigger = {
			NOT = { exists = var:char91 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle92 = {
	type = character

	text = {
		trigger = {
			exists = var:char92
			NOT = { var:char92 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.92
	}
	text = {
		trigger = {
			NOT = { exists = var:char92 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle93 = {
	type = character

	text = {
		trigger = {
			exists = var:char93
			NOT = { var:char93 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.93
	}
	text = {
		trigger = {
			NOT = { exists = var:char93 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle94 = {
	type = character

	text = {
		trigger = {
			exists = var:char94
			NOT = { var:char94 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.94
	}
	text = {
		trigger = {
			NOT = { exists = var:char94 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle95 = {
	type = character

	text = {
		trigger = {
			exists = var:char95
			NOT = { var:char95 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.95
	}
	text = {
		trigger = {
			NOT = { exists = var:char95 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle96 = {
	type = character

	text = {
		trigger = {
			exists = var:char96
			NOT = { var:char96 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.96
	}
	text = {
		trigger = {
			NOT = { exists = var:char96 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle97 = {
	type = character

	text = {
		trigger = {
			exists = var:char97
			NOT = { var:char97 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.97
	}
	text = {
		trigger = {
			NOT = { exists = var:char97 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle98 = {
	type = character

	text = {
		trigger = {
			exists = var:char98
			NOT = { var:char98 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.98
	}
	text = {
		trigger = {
			NOT = { exists = var:char98 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle99 = {
	type = character

	text = {
		trigger = {
			exists = var:char99
			NOT = { var:char99 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.99
	}
	text = {
		trigger = {
			NOT = { exists = var:char99 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}

ChronicleCharacterNameNoTitle100 = {
	type = character

	text = {
		trigger = {
			exists = var:char100
			NOT = { var:char100 = root } # Stand for nobody
		}
		localization_key = chronicle_contents_character_name_no_title.100
	}
	text = {
		trigger = {
			NOT = { exists = var:char100 }
		}
		localization_key = chronicle_contents_character_name_no_title.0
	}
}
