﻿ChronicleFlag1 = {
	type = character

	text = {
		trigger = {
			exists = var:type1
			exists = var:flag_b1
			var:type1 = flag:title
			NOT = { var:flag_b1 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.1
	}
	text = {
		trigger = {
			exists = var:type1
			var:type1 = flag:war
		}
		localization_key = chronicle_contents_flag_war.1
	}
}

ChronicleFlag2 = {
	type = character

	text = {
		trigger = {
			exists = var:type2
			exists = var:flag_b2
			var:type2 = flag:title
			NOT = { var:flag_b2 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.2
	}
	text = {
		trigger = {
			exists = var:type2
			var:type2 = flag:war
		}
		localization_key = chronicle_contents_flag_war.2
	}
}

ChronicleFlag3 = {
	type = character

	text = {
		trigger = {
			exists = var:type3
			exists = var:flag_b3
			var:type3 = flag:title
			NOT = { var:flag_b3 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.3
	}
	text = {
		trigger = {
			exists = var:type3
			var:type3 = flag:war
		}
		localization_key = chronicle_contents_flag_war.3
	}
}

ChronicleFlag4 = {
	type = character

	text = {
		trigger = {
			exists = var:type4
			exists = var:flag_b4
			var:type4 = flag:title
			NOT = { var:flag_b4 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.4
	}
	text = {
		trigger = {
			exists = var:type4
			var:type4 = flag:war
		}
		localization_key = chronicle_contents_flag_war.4
	}
}

ChronicleFlag5 = {
	type = character

	text = {
		trigger = {
			exists = var:type5
			exists = var:flag_b5
			var:type5 = flag:title
			NOT = { var:flag_b5 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.5
	}
	text = {
		trigger = {
			exists = var:type5
			var:type5 = flag:war
		}
		localization_key = chronicle_contents_flag_war.5
	}
}

ChronicleFlag6 = {
	type = character

	text = {
		trigger = {
			exists = var:type6
			exists = var:flag_b6
			var:type6 = flag:title
			NOT = { var:flag_b6 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.6
	}
	text = {
		trigger = {
			exists = var:type6
			var:type6 = flag:war
		}
		localization_key = chronicle_contents_flag_war.6
	}
}

ChronicleFlag7 = {
	type = character

	text = {
		trigger = {
			exists = var:type7
			exists = var:flag_b7
			var:type7 = flag:title
			NOT = { var:flag_b7 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.7
	}
	text = {
		trigger = {
			exists = var:type7
			var:type7 = flag:war
		}
		localization_key = chronicle_contents_flag_war.7
	}
}

ChronicleFlag8 = {
	type = character

	text = {
		trigger = {
			exists = var:type8
			exists = var:flag_b8
			var:type8 = flag:title
			NOT = { var:flag_b8 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.8
	}
	text = {
		trigger = {
			exists = var:type8
			var:type8 = flag:war
		}
		localization_key = chronicle_contents_flag_war.8
	}
}

ChronicleFlag9 = {
	type = character

	text = {
		trigger = {
			exists = var:type9
			exists = var:flag_b9
			var:type9 = flag:title
			NOT = { var:flag_b9 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.9
	}
	text = {
		trigger = {
			exists = var:type9
			var:type9 = flag:war
		}
		localization_key = chronicle_contents_flag_war.9
	}
}

ChronicleFlag10 = {
	type = character

	text = {
		trigger = {
			exists = var:type10
			exists = var:flag_b10
			var:type10 = flag:title
			NOT = { var:flag_b10 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.10
	}
	text = {
		trigger = {
			exists = var:type10
			var:type10 = flag:war
		}
		localization_key = chronicle_contents_flag_war.10
	}
}

ChronicleFlag11 = {
	type = character

	text = {
		trigger = {
			exists = var:type11
			exists = var:flag_b11
			var:type11 = flag:title
			NOT = { var:flag_b11 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.11
	}
	text = {
		trigger = {
			exists = var:type11
			var:type11 = flag:war
		}
		localization_key = chronicle_contents_flag_war.11
	}
}

ChronicleFlag12 = {
	type = character

	text = {
		trigger = {
			exists = var:type12
			exists = var:flag_b12
			var:type12 = flag:title
			NOT = { var:flag_b12 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.12
	}
	text = {
		trigger = {
			exists = var:type12
			var:type12 = flag:war
		}
		localization_key = chronicle_contents_flag_war.12
	}
}

ChronicleFlag13 = {
	type = character

	text = {
		trigger = {
			exists = var:type13
			exists = var:flag_b13
			var:type13 = flag:title
			NOT = { var:flag_b13 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.13
	}
	text = {
		trigger = {
			exists = var:type13
			var:type13 = flag:war
		}
		localization_key = chronicle_contents_flag_war.13
	}
}

ChronicleFlag14 = {
	type = character

	text = {
		trigger = {
			exists = var:type14
			exists = var:flag_b14
			var:type14 = flag:title
			NOT = { var:flag_b14 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.14
	}
	text = {
		trigger = {
			exists = var:type14
			var:type14 = flag:war
		}
		localization_key = chronicle_contents_flag_war.14
	}
}

ChronicleFlag15 = {
	type = character

	text = {
		trigger = {
			exists = var:type15
			exists = var:flag_b15
			var:type15 = flag:title
			NOT = { var:flag_b15 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.15
	}
	text = {
		trigger = {
			exists = var:type15
			var:type15 = flag:war
		}
		localization_key = chronicle_contents_flag_war.15
	}
}

ChronicleFlag16 = {
	type = character

	text = {
		trigger = {
			exists = var:type16
			exists = var:flag_b16
			var:type16 = flag:title
			NOT = { var:flag_b16 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.16
	}
	text = {
		trigger = {
			exists = var:type16
			var:type16 = flag:war
		}
		localization_key = chronicle_contents_flag_war.16
	}
}

ChronicleFlag17 = {
	type = character

	text = {
		trigger = {
			exists = var:type17
			exists = var:flag_b17
			var:type17 = flag:title
			NOT = { var:flag_b17 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.17
	}
	text = {
		trigger = {
			exists = var:type17
			var:type17 = flag:war
		}
		localization_key = chronicle_contents_flag_war.17
	}
}

ChronicleFlag18 = {
	type = character

	text = {
		trigger = {
			exists = var:type18
			exists = var:flag_b18
			var:type18 = flag:title
			NOT = { var:flag_b18 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.18
	}
	text = {
		trigger = {
			exists = var:type18
			var:type18 = flag:war
		}
		localization_key = chronicle_contents_flag_war.18
	}
}

ChronicleFlag19 = {
	type = character

	text = {
		trigger = {
			exists = var:type19
			exists = var:flag_b19
			var:type19 = flag:title
			NOT = { var:flag_b19 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.19
	}
	text = {
		trigger = {
			exists = var:type19
			var:type19 = flag:war
		}
		localization_key = chronicle_contents_flag_war.19
	}
}

ChronicleFlag20 = {
	type = character

	text = {
		trigger = {
			exists = var:type20
			exists = var:flag_b20
			var:type20 = flag:title
			NOT = { var:flag_b20 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.20
	}
	text = {
		trigger = {
			exists = var:type20
			var:type20 = flag:war
		}
		localization_key = chronicle_contents_flag_war.20
	}
}

ChronicleFlag21 = {
	type = character

	text = {
		trigger = {
			exists = var:type21
			exists = var:flag_b21
			var:type21 = flag:title
			NOT = { var:flag_b21 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.21
	}
	text = {
		trigger = {
			exists = var:type21
			var:type21 = flag:war
		}
		localization_key = chronicle_contents_flag_war.21
	}
}

ChronicleFlag22 = {
	type = character

	text = {
		trigger = {
			exists = var:type22
			exists = var:flag_b22
			var:type22 = flag:title
			NOT = { var:flag_b22 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.22
	}
	text = {
		trigger = {
			exists = var:type22
			var:type22 = flag:war
		}
		localization_key = chronicle_contents_flag_war.22
	}
}

ChronicleFlag23 = {
	type = character

	text = {
		trigger = {
			exists = var:type23
			exists = var:flag_b23
			var:type23 = flag:title
			NOT = { var:flag_b23 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.23
	}
	text = {
		trigger = {
			exists = var:type23
			var:type23 = flag:war
		}
		localization_key = chronicle_contents_flag_war.23
	}
}

ChronicleFlag24 = {
	type = character

	text = {
		trigger = {
			exists = var:type24
			exists = var:flag_b24
			var:type24 = flag:title
			NOT = { var:flag_b24 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.24
	}
	text = {
		trigger = {
			exists = var:type24
			var:type24 = flag:war
		}
		localization_key = chronicle_contents_flag_war.24
	}
}

ChronicleFlag25 = {
	type = character

	text = {
		trigger = {
			exists = var:type25
			exists = var:flag_b25
			var:type25 = flag:title
			NOT = { var:flag_b25 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.25
	}
	text = {
		trigger = {
			exists = var:type25
			var:type25 = flag:war
		}
		localization_key = chronicle_contents_flag_war.25
	}
}

ChronicleFlag26 = {
	type = character

	text = {
		trigger = {
			exists = var:type26
			exists = var:flag_b26
			var:type26 = flag:title
			NOT = { var:flag_b26 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.26
	}
	text = {
		trigger = {
			exists = var:type26
			var:type26 = flag:war
		}
		localization_key = chronicle_contents_flag_war.26
	}
}

ChronicleFlag27 = {
	type = character

	text = {
		trigger = {
			exists = var:type27
			exists = var:flag_b27
			var:type27 = flag:title
			NOT = { var:flag_b27 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.27
	}
	text = {
		trigger = {
			exists = var:type27
			var:type27 = flag:war
		}
		localization_key = chronicle_contents_flag_war.27
	}
}

ChronicleFlag28 = {
	type = character

	text = {
		trigger = {
			exists = var:type28
			exists = var:flag_b28
			var:type28 = flag:title
			NOT = { var:flag_b28 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.28
	}
	text = {
		trigger = {
			exists = var:type28
			var:type28 = flag:war
		}
		localization_key = chronicle_contents_flag_war.28
	}
}

ChronicleFlag29 = {
	type = character

	text = {
		trigger = {
			exists = var:type29
			exists = var:flag_b29
			var:type29 = flag:title
			NOT = { var:flag_b29 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.29
	}
	text = {
		trigger = {
			exists = var:type29
			var:type29 = flag:war
		}
		localization_key = chronicle_contents_flag_war.29
	}
}

ChronicleFlag30 = {
	type = character

	text = {
		trigger = {
			exists = var:type30
			exists = var:flag_b30
			var:type30 = flag:title
			NOT = { var:flag_b30 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.30
	}
	text = {
		trigger = {
			exists = var:type30
			var:type30 = flag:war
		}
		localization_key = chronicle_contents_flag_war.30
	}
}

ChronicleFlag31 = {
	type = character

	text = {
		trigger = {
			exists = var:type31
			exists = var:flag_b31
			var:type31 = flag:title
			NOT = { var:flag_b31 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.31
	}
	text = {
		trigger = {
			exists = var:type31
			var:type31 = flag:war
		}
		localization_key = chronicle_contents_flag_war.31
	}
}

ChronicleFlag32 = {
	type = character

	text = {
		trigger = {
			exists = var:type32
			exists = var:flag_b32
			var:type32 = flag:title
			NOT = { var:flag_b32 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.32
	}
	text = {
		trigger = {
			exists = var:type32
			var:type32 = flag:war
		}
		localization_key = chronicle_contents_flag_war.32
	}
}

ChronicleFlag33 = {
	type = character

	text = {
		trigger = {
			exists = var:type33
			exists = var:flag_b33
			var:type33 = flag:title
			NOT = { var:flag_b33 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.33
	}
	text = {
		trigger = {
			exists = var:type33
			var:type33 = flag:war
		}
		localization_key = chronicle_contents_flag_war.33
	}
}

ChronicleFlag34 = {
	type = character

	text = {
		trigger = {
			exists = var:type34
			exists = var:flag_b34
			var:type34 = flag:title
			NOT = { var:flag_b34 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.34
	}
	text = {
		trigger = {
			exists = var:type34
			var:type34 = flag:war
		}
		localization_key = chronicle_contents_flag_war.34
	}
}

ChronicleFlag35 = {
	type = character

	text = {
		trigger = {
			exists = var:type35
			exists = var:flag_b35
			var:type35 = flag:title
			NOT = { var:flag_b35 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.35
	}
	text = {
		trigger = {
			exists = var:type35
			var:type35 = flag:war
		}
		localization_key = chronicle_contents_flag_war.35
	}
}

ChronicleFlag36 = {
	type = character

	text = {
		trigger = {
			exists = var:type36
			exists = var:flag_b36
			var:type36 = flag:title
			NOT = { var:flag_b36 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.36
	}
	text = {
		trigger = {
			exists = var:type36
			var:type36 = flag:war
		}
		localization_key = chronicle_contents_flag_war.36
	}
}

ChronicleFlag37 = {
	type = character

	text = {
		trigger = {
			exists = var:type37
			exists = var:flag_b37
			var:type37 = flag:title
			NOT = { var:flag_b37 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.37
	}
	text = {
		trigger = {
			exists = var:type37
			var:type37 = flag:war
		}
		localization_key = chronicle_contents_flag_war.37
	}
}

ChronicleFlag38 = {
	type = character

	text = {
		trigger = {
			exists = var:type38
			exists = var:flag_b38
			var:type38 = flag:title
			NOT = { var:flag_b38 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.38
	}
	text = {
		trigger = {
			exists = var:type38
			var:type38 = flag:war
		}
		localization_key = chronicle_contents_flag_war.38
	}
}

ChronicleFlag39 = {
	type = character

	text = {
		trigger = {
			exists = var:type39
			exists = var:flag_b39
			var:type39 = flag:title
			NOT = { var:flag_b39 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.39
	}
	text = {
		trigger = {
			exists = var:type39
			var:type39 = flag:war
		}
		localization_key = chronicle_contents_flag_war.39
	}
}

ChronicleFlag40 = {
	type = character

	text = {
		trigger = {
			exists = var:type40
			exists = var:flag_b40
			var:type40 = flag:title
			NOT = { var:flag_b40 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.40
	}
	text = {
		trigger = {
			exists = var:type40
			var:type40 = flag:war
		}
		localization_key = chronicle_contents_flag_war.40
	}
}

ChronicleFlag41 = {
	type = character

	text = {
		trigger = {
			exists = var:type41
			exists = var:flag_b41
			var:type41 = flag:title
			NOT = { var:flag_b41 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.41
	}
	text = {
		trigger = {
			exists = var:type41
			var:type41 = flag:war
		}
		localization_key = chronicle_contents_flag_war.41
	}
}

ChronicleFlag42 = {
	type = character

	text = {
		trigger = {
			exists = var:type42
			exists = var:flag_b42
			var:type42 = flag:title
			NOT = { var:flag_b42 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.42
	}
	text = {
		trigger = {
			exists = var:type42
			var:type42 = flag:war
		}
		localization_key = chronicle_contents_flag_war.42
	}
}

ChronicleFlag43 = {
	type = character

	text = {
		trigger = {
			exists = var:type43
			exists = var:flag_b43
			var:type43 = flag:title
			NOT = { var:flag_b43 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.43
	}
	text = {
		trigger = {
			exists = var:type43
			var:type43 = flag:war
		}
		localization_key = chronicle_contents_flag_war.43
	}
}

ChronicleFlag44 = {
	type = character

	text = {
		trigger = {
			exists = var:type44
			exists = var:flag_b44
			var:type44 = flag:title
			NOT = { var:flag_b44 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.44
	}
	text = {
		trigger = {
			exists = var:type44
			var:type44 = flag:war
		}
		localization_key = chronicle_contents_flag_war.44
	}
}

ChronicleFlag45 = {
	type = character

	text = {
		trigger = {
			exists = var:type45
			exists = var:flag_b45
			var:type45 = flag:title
			NOT = { var:flag_b45 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.45
	}
	text = {
		trigger = {
			exists = var:type45
			var:type45 = flag:war
		}
		localization_key = chronicle_contents_flag_war.45
	}
}

ChronicleFlag46 = {
	type = character

	text = {
		trigger = {
			exists = var:type46
			exists = var:flag_b46
			var:type46 = flag:title
			NOT = { var:flag_b46 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.46
	}
	text = {
		trigger = {
			exists = var:type46
			var:type46 = flag:war
		}
		localization_key = chronicle_contents_flag_war.46
	}
}

ChronicleFlag47 = {
	type = character

	text = {
		trigger = {
			exists = var:type47
			exists = var:flag_b47
			var:type47 = flag:title
			NOT = { var:flag_b47 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.47
	}
	text = {
		trigger = {
			exists = var:type47
			var:type47 = flag:war
		}
		localization_key = chronicle_contents_flag_war.47
	}
}

ChronicleFlag48 = {
	type = character

	text = {
		trigger = {
			exists = var:type48
			exists = var:flag_b48
			var:type48 = flag:title
			NOT = { var:flag_b48 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.48
	}
	text = {
		trigger = {
			exists = var:type48
			var:type48 = flag:war
		}
		localization_key = chronicle_contents_flag_war.48
	}
}

ChronicleFlag49 = {
	type = character

	text = {
		trigger = {
			exists = var:type49
			exists = var:flag_b49
			var:type49 = flag:title
			NOT = { var:flag_b49 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.49
	}
	text = {
		trigger = {
			exists = var:type49
			var:type49 = flag:war
		}
		localization_key = chronicle_contents_flag_war.49
	}
}

ChronicleFlag50 = {
	type = character

	text = {
		trigger = {
			exists = var:type50
			exists = var:flag_b50
			var:type50 = flag:title
			NOT = { var:flag_b50 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.50
	}
	text = {
		trigger = {
			exists = var:type50
			var:type50 = flag:war
		}
		localization_key = chronicle_contents_flag_war.50
	}
}

ChronicleFlag51 = {
	type = character

	text = {
		trigger = {
			exists = var:type51
			exists = var:flag_b51
			var:type51 = flag:title
			NOT = { var:flag_b51 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.51
	}
	text = {
		trigger = {
			exists = var:type51
			var:type51 = flag:war
		}
		localization_key = chronicle_contents_flag_war.51
	}
}

ChronicleFlag52 = {
	type = character

	text = {
		trigger = {
			exists = var:type52
			exists = var:flag_b52
			var:type52 = flag:title
			NOT = { var:flag_b52 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.52
	}
	text = {
		trigger = {
			exists = var:type52
			var:type52 = flag:war
		}
		localization_key = chronicle_contents_flag_war.52
	}
}

ChronicleFlag53 = {
	type = character

	text = {
		trigger = {
			exists = var:type53
			exists = var:flag_b53
			var:type53 = flag:title
			NOT = { var:flag_b53 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.53
	}
	text = {
		trigger = {
			exists = var:type53
			var:type53 = flag:war
		}
		localization_key = chronicle_contents_flag_war.53
	}
}

ChronicleFlag54 = {
	type = character

	text = {
		trigger = {
			exists = var:type54
			exists = var:flag_b54
			var:type54 = flag:title
			NOT = { var:flag_b54 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.54
	}
	text = {
		trigger = {
			exists = var:type54
			var:type54 = flag:war
		}
		localization_key = chronicle_contents_flag_war.54
	}
}

ChronicleFlag55 = {
	type = character

	text = {
		trigger = {
			exists = var:type55
			exists = var:flag_b55
			var:type55 = flag:title
			NOT = { var:flag_b55 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.55
	}
	text = {
		trigger = {
			exists = var:type55
			var:type55 = flag:war
		}
		localization_key = chronicle_contents_flag_war.55
	}
}

ChronicleFlag56 = {
	type = character

	text = {
		trigger = {
			exists = var:type56
			exists = var:flag_b56
			var:type56 = flag:title
			NOT = { var:flag_b56 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.56
	}
	text = {
		trigger = {
			exists = var:type56
			var:type56 = flag:war
		}
		localization_key = chronicle_contents_flag_war.56
	}
}

ChronicleFlag57 = {
	type = character

	text = {
		trigger = {
			exists = var:type57
			exists = var:flag_b57
			var:type57 = flag:title
			NOT = { var:flag_b57 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.57
	}
	text = {
		trigger = {
			exists = var:type57
			var:type57 = flag:war
		}
		localization_key = chronicle_contents_flag_war.57
	}
}

ChronicleFlag58 = {
	type = character

	text = {
		trigger = {
			exists = var:type58
			exists = var:flag_b58
			var:type58 = flag:title
			NOT = { var:flag_b58 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.58
	}
	text = {
		trigger = {
			exists = var:type58
			var:type58 = flag:war
		}
		localization_key = chronicle_contents_flag_war.58
	}
}

ChronicleFlag59 = {
	type = character

	text = {
		trigger = {
			exists = var:type59
			exists = var:flag_b59
			var:type59 = flag:title
			NOT = { var:flag_b59 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.59
	}
	text = {
		trigger = {
			exists = var:type59
			var:type59 = flag:war
		}
		localization_key = chronicle_contents_flag_war.59
	}
}

ChronicleFlag60 = {
	type = character

	text = {
		trigger = {
			exists = var:type60
			exists = var:flag_b60
			var:type60 = flag:title
			NOT = { var:flag_b60 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.60
	}
	text = {
		trigger = {
			exists = var:type60
			var:type60 = flag:war
		}
		localization_key = chronicle_contents_flag_war.60
	}
}

ChronicleFlag61 = {
	type = character

	text = {
		trigger = {
			exists = var:type61
			exists = var:flag_b61
			var:type61 = flag:title
			NOT = { var:flag_b61 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.61
	}
	text = {
		trigger = {
			exists = var:type61
			var:type61 = flag:war
		}
		localization_key = chronicle_contents_flag_war.61
	}
}

ChronicleFlag62 = {
	type = character

	text = {
		trigger = {
			exists = var:type62
			exists = var:flag_b62
			var:type62 = flag:title
			NOT = { var:flag_b62 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.62
	}
	text = {
		trigger = {
			exists = var:type62
			var:type62 = flag:war
		}
		localization_key = chronicle_contents_flag_war.62
	}
}

ChronicleFlag63 = {
	type = character

	text = {
		trigger = {
			exists = var:type63
			exists = var:flag_b63
			var:type63 = flag:title
			NOT = { var:flag_b63 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.63
	}
	text = {
		trigger = {
			exists = var:type63
			var:type63 = flag:war
		}
		localization_key = chronicle_contents_flag_war.63
	}
}

ChronicleFlag64 = {
	type = character

	text = {
		trigger = {
			exists = var:type64
			exists = var:flag_b64
			var:type64 = flag:title
			NOT = { var:flag_b64 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.64
	}
	text = {
		trigger = {
			exists = var:type64
			var:type64 = flag:war
		}
		localization_key = chronicle_contents_flag_war.64
	}
}

ChronicleFlag65 = {
	type = character

	text = {
		trigger = {
			exists = var:type65
			exists = var:flag_b65
			var:type65 = flag:title
			NOT = { var:flag_b65 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.65
	}
	text = {
		trigger = {
			exists = var:type65
			var:type65 = flag:war
		}
		localization_key = chronicle_contents_flag_war.65
	}
}

ChronicleFlag66 = {
	type = character

	text = {
		trigger = {
			exists = var:type66
			exists = var:flag_b66
			var:type66 = flag:title
			NOT = { var:flag_b66 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.66
	}
	text = {
		trigger = {
			exists = var:type66
			var:type66 = flag:war
		}
		localization_key = chronicle_contents_flag_war.66
	}
}

ChronicleFlag67 = {
	type = character

	text = {
		trigger = {
			exists = var:type67
			exists = var:flag_b67
			var:type67 = flag:title
			NOT = { var:flag_b67 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.67
	}
	text = {
		trigger = {
			exists = var:type67
			var:type67 = flag:war
		}
		localization_key = chronicle_contents_flag_war.67
	}
}

ChronicleFlag68 = {
	type = character

	text = {
		trigger = {
			exists = var:type68
			exists = var:flag_b68
			var:type68 = flag:title
			NOT = { var:flag_b68 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.68
	}
	text = {
		trigger = {
			exists = var:type68
			var:type68 = flag:war
		}
		localization_key = chronicle_contents_flag_war.68
	}
}

ChronicleFlag69 = {
	type = character

	text = {
		trigger = {
			exists = var:type69
			exists = var:flag_b69
			var:type69 = flag:title
			NOT = { var:flag_b69 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.69
	}
	text = {
		trigger = {
			exists = var:type69
			var:type69 = flag:war
		}
		localization_key = chronicle_contents_flag_war.69
	}
}

ChronicleFlag70 = {
	type = character

	text = {
		trigger = {
			exists = var:type70
			exists = var:flag_b70
			var:type70 = flag:title
			NOT = { var:flag_b70 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.70
	}
	text = {
		trigger = {
			exists = var:type70
			var:type70 = flag:war
		}
		localization_key = chronicle_contents_flag_war.70
	}
}

ChronicleFlag71 = {
	type = character

	text = {
		trigger = {
			exists = var:type71
			exists = var:flag_b71
			var:type71 = flag:title
			NOT = { var:flag_b71 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.71
	}
	text = {
		trigger = {
			exists = var:type71
			var:type71 = flag:war
		}
		localization_key = chronicle_contents_flag_war.71
	}
}

ChronicleFlag72 = {
	type = character

	text = {
		trigger = {
			exists = var:type72
			exists = var:flag_b72
			var:type72 = flag:title
			NOT = { var:flag_b72 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.72
	}
	text = {
		trigger = {
			exists = var:type72
			var:type72 = flag:war
		}
		localization_key = chronicle_contents_flag_war.72
	}
}

ChronicleFlag73 = {
	type = character

	text = {
		trigger = {
			exists = var:type73
			exists = var:flag_b73
			var:type73 = flag:title
			NOT = { var:flag_b73 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.73
	}
	text = {
		trigger = {
			exists = var:type73
			var:type73 = flag:war
		}
		localization_key = chronicle_contents_flag_war.73
	}
}

ChronicleFlag74 = {
	type = character

	text = {
		trigger = {
			exists = var:type74
			exists = var:flag_b74
			var:type74 = flag:title
			NOT = { var:flag_b74 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.74
	}
	text = {
		trigger = {
			exists = var:type74
			var:type74 = flag:war
		}
		localization_key = chronicle_contents_flag_war.74
	}
}

ChronicleFlag75 = {
	type = character

	text = {
		trigger = {
			exists = var:type75
			exists = var:flag_b75
			var:type75 = flag:title
			NOT = { var:flag_b75 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.75
	}
	text = {
		trigger = {
			exists = var:type75
			var:type75 = flag:war
		}
		localization_key = chronicle_contents_flag_war.75
	}
}

ChronicleFlag76 = {
	type = character

	text = {
		trigger = {
			exists = var:type76
			exists = var:flag_b76
			var:type76 = flag:title
			NOT = { var:flag_b76 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.76
	}
	text = {
		trigger = {
			exists = var:type76
			var:type76 = flag:war
		}
		localization_key = chronicle_contents_flag_war.76
	}
}

ChronicleFlag77 = {
	type = character

	text = {
		trigger = {
			exists = var:type77
			exists = var:flag_b77
			var:type77 = flag:title
			NOT = { var:flag_b77 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.77
	}
	text = {
		trigger = {
			exists = var:type77
			var:type77 = flag:war
		}
		localization_key = chronicle_contents_flag_war.77
	}
}

ChronicleFlag78 = {
	type = character

	text = {
		trigger = {
			exists = var:type78
			exists = var:flag_b78
			var:type78 = flag:title
			NOT = { var:flag_b78 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.78
	}
	text = {
		trigger = {
			exists = var:type78
			var:type78 = flag:war
		}
		localization_key = chronicle_contents_flag_war.78
	}
}

ChronicleFlag79 = {
	type = character

	text = {
		trigger = {
			exists = var:type79
			exists = var:flag_b79
			var:type79 = flag:title
			NOT = { var:flag_b79 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.79
	}
	text = {
		trigger = {
			exists = var:type79
			var:type79 = flag:war
		}
		localization_key = chronicle_contents_flag_war.79
	}
}

ChronicleFlag80 = {
	type = character

	text = {
		trigger = {
			exists = var:type80
			exists = var:flag_b80
			var:type80 = flag:title
			NOT = { var:flag_b80 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.80
	}
	text = {
		trigger = {
			exists = var:type80
			var:type80 = flag:war
		}
		localization_key = chronicle_contents_flag_war.80
	}
}

ChronicleFlag81 = {
	type = character

	text = {
		trigger = {
			exists = var:type81
			exists = var:flag_b81
			var:type81 = flag:title
			NOT = { var:flag_b81 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.81
	}
	text = {
		trigger = {
			exists = var:type81
			var:type81 = flag:war
		}
		localization_key = chronicle_contents_flag_war.81
	}
}

ChronicleFlag82 = {
	type = character

	text = {
		trigger = {
			exists = var:type82
			exists = var:flag_b82
			var:type82 = flag:title
			NOT = { var:flag_b82 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.82
	}
	text = {
		trigger = {
			exists = var:type82
			var:type82 = flag:war
		}
		localization_key = chronicle_contents_flag_war.82
	}
}

ChronicleFlag83 = {
	type = character

	text = {
		trigger = {
			exists = var:type83
			exists = var:flag_b83
			var:type83 = flag:title
			NOT = { var:flag_b83 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.83
	}
	text = {
		trigger = {
			exists = var:type83
			var:type83 = flag:war
		}
		localization_key = chronicle_contents_flag_war.83
	}
}

ChronicleFlag84 = {
	type = character

	text = {
		trigger = {
			exists = var:type84
			exists = var:flag_b84
			var:type84 = flag:title
			NOT = { var:flag_b84 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.84
	}
	text = {
		trigger = {
			exists = var:type84
			var:type84 = flag:war
		}
		localization_key = chronicle_contents_flag_war.84
	}
}

ChronicleFlag85 = {
	type = character

	text = {
		trigger = {
			exists = var:type85
			exists = var:flag_b85
			var:type85 = flag:title
			NOT = { var:flag_b85 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.85
	}
	text = {
		trigger = {
			exists = var:type85
			var:type85 = flag:war
		}
		localization_key = chronicle_contents_flag_war.85
	}
}

ChronicleFlag86 = {
	type = character

	text = {
		trigger = {
			exists = var:type86
			exists = var:flag_b86
			var:type86 = flag:title
			NOT = { var:flag_b86 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.86
	}
	text = {
		trigger = {
			exists = var:type86
			var:type86 = flag:war
		}
		localization_key = chronicle_contents_flag_war.86
	}
}

ChronicleFlag87 = {
	type = character

	text = {
		trigger = {
			exists = var:type87
			exists = var:flag_b87
			var:type87 = flag:title
			NOT = { var:flag_b87 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.87
	}
	text = {
		trigger = {
			exists = var:type87
			var:type87 = flag:war
		}
		localization_key = chronicle_contents_flag_war.87
	}
}

ChronicleFlag88 = {
	type = character

	text = {
		trigger = {
			exists = var:type88
			exists = var:flag_b88
			var:type88 = flag:title
			NOT = { var:flag_b88 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.88
	}
	text = {
		trigger = {
			exists = var:type88
			var:type88 = flag:war
		}
		localization_key = chronicle_contents_flag_war.88
	}
}

ChronicleFlag89 = {
	type = character

	text = {
		trigger = {
			exists = var:type89
			exists = var:flag_b89
			var:type89 = flag:title
			NOT = { var:flag_b89 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.89
	}
	text = {
		trigger = {
			exists = var:type89
			var:type89 = flag:war
		}
		localization_key = chronicle_contents_flag_war.89
	}
}

ChronicleFlag90 = {
	type = character

	text = {
		trigger = {
			exists = var:type90
			exists = var:flag_b90
			var:type90 = flag:title
			NOT = { var:flag_b90 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.90
	}
	text = {
		trigger = {
			exists = var:type90
			var:type90 = flag:war
		}
		localization_key = chronicle_contents_flag_war.90
	}
}

ChronicleFlag91 = {
	type = character

	text = {
		trigger = {
			exists = var:type91
			exists = var:flag_b91
			var:type91 = flag:title
			NOT = { var:flag_b91 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.91
	}
	text = {
		trigger = {
			exists = var:type91
			var:type91 = flag:war
		}
		localization_key = chronicle_contents_flag_war.91
	}
}

ChronicleFlag92 = {
	type = character

	text = {
		trigger = {
			exists = var:type92
			exists = var:flag_b92
			var:type92 = flag:title
			NOT = { var:flag_b92 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.92
	}
	text = {
		trigger = {
			exists = var:type92
			var:type92 = flag:war
		}
		localization_key = chronicle_contents_flag_war.92
	}
}

ChronicleFlag93 = {
	type = character

	text = {
		trigger = {
			exists = var:type93
			exists = var:flag_b93
			var:type93 = flag:title
			NOT = { var:flag_b93 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.93
	}
	text = {
		trigger = {
			exists = var:type93
			var:type93 = flag:war
		}
		localization_key = chronicle_contents_flag_war.93
	}
}

ChronicleFlag94 = {
	type = character

	text = {
		trigger = {
			exists = var:type94
			exists = var:flag_b94
			var:type94 = flag:title
			NOT = { var:flag_b94 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.94
	}
	text = {
		trigger = {
			exists = var:type94
			var:type94 = flag:war
		}
		localization_key = chronicle_contents_flag_war.94
	}
}

ChronicleFlag95 = {
	type = character

	text = {
		trigger = {
			exists = var:type95
			exists = var:flag_b95
			var:type95 = flag:title
			NOT = { var:flag_b95 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.95
	}
	text = {
		trigger = {
			exists = var:type95
			var:type95 = flag:war
		}
		localization_key = chronicle_contents_flag_war.95
	}
}

ChronicleFlag96 = {
	type = character

	text = {
		trigger = {
			exists = var:type96
			exists = var:flag_b96
			var:type96 = flag:title
			NOT = { var:flag_b96 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.96
	}
	text = {
		trigger = {
			exists = var:type96
			var:type96 = flag:war
		}
		localization_key = chronicle_contents_flag_war.96
	}
}

ChronicleFlag97 = {
	type = character

	text = {
		trigger = {
			exists = var:type97
			exists = var:flag_b97
			var:type97 = flag:title
			NOT = { var:flag_b97 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.97
	}
	text = {
		trigger = {
			exists = var:type97
			var:type97 = flag:war
		}
		localization_key = chronicle_contents_flag_war.97
	}
}

ChronicleFlag98 = {
	type = character

	text = {
		trigger = {
			exists = var:type98
			exists = var:flag_b98
			var:type98 = flag:title
			NOT = { var:flag_b98 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.98
	}
	text = {
		trigger = {
			exists = var:type98
			var:type98 = flag:war
		}
		localization_key = chronicle_contents_flag_war.98
	}
}

ChronicleFlag99 = {
	type = character

	text = {
		trigger = {
			exists = var:type99
			exists = var:flag_b99
			var:type99 = flag:title
			NOT = { var:flag_b99 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.99
	}
	text = {
		trigger = {
			exists = var:type99
			var:type99 = flag:war
		}
		localization_key = chronicle_contents_flag_war.99
	}
}

ChronicleFlag100 = {
	type = character

	text = {
		trigger = {
			exists = var:type100
			exists = var:flag_b100
			var:type100 = flag:title
			NOT = { var:flag_b100 = flag:none }
		}
		localization_key = chronicle_contents_flag_title.100
	}
	text = {
		trigger = {
			exists = var:type100
			var:type100 = flag:war
		}
		localization_key = chronicle_contents_flag_war.100
	}
}
