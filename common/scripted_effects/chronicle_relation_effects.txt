﻿chronicle_relation_change_effect = {

	root         = { save_scope_as = person_1 }
	scope:target = { save_scope_as = person_2 }

	chronicle_set_player_scope_effect = yes

	if = {

		limit = {
			exists = global_var:chronicle_mod_version
			global_var:chronicle_mod_version >= 1.30
			OR = {
				chronicle_is_recordable_trigger = { ROOT = scope:person_1 }
				chronicle_is_recordable_trigger = { ROOT = scope:person_2 }
			}
		}

		scope:person_1 = { save_scope_as = recipient }
		scope:recipient = {
			set_chronicle_date_effect = yes
			set_chronicle_contents_effect = {
				TYPE   = relation
				CHAR   = scope:person_2
				TITLE  = scope:person_2.primary_title
				FLAG_A = flag:$GAIN_OR_LOSE$
				FLAG_B = flag:$RELATION$
				FLAG_C = flag:none
				FLAG_D = flag:none
				FLAG_E = flag:none
			}
			reset_chronicle_date_effect = yes
		}

		scope:person_2 = { save_scope_as = recipient }
		scope:recipient = {
			set_chronicle_date_effect = yes
			set_chronicle_contents_effect = {
				TYPE   = relation
				CHAR   = scope:person_1
				TITLE  = scope:person_1.primary_title
				FLAG_A = flag:$GAIN_OR_LOSE$
				FLAG_B = flag:$RELATION$
				FLAG_C = flag:none
				FLAG_D = flag:none
				FLAG_E = flag:none
			}
			reset_chronicle_date_effect = yes
		}

	}

}


chronicle_relation_death_effect = {

	if = {

		limit = {
			exists = global_var:chronicle_mod_version
			global_var:chronicle_mod_version >= 1.30
		}

		scope:recipient = {
			set_chronicle_date_effect = yes
			set_chronicle_contents_effect = {
				TYPE   = relation
				CHAR   = scope:dying
				TITLE  = scope:dying.primary_title
				FLAG_A = flag:lose
				FLAG_B = flag:$RELATION$
				FLAG_C = flag:none
				FLAG_D = flag:none
				FLAG_E = flag:none
			}
			reset_chronicle_date_effect = yes
		}

	}

}
